import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';
import AutoImport from 'unplugin-auto-import/vite';
import { resolve } from 'path';

// 路径查找
const pathResolve = (dir: string): string => {
  return resolve(__dirname, dir);
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    uni(),
    //自动加载依赖
    AutoImport({
      imports: ['vue','uni-app'],
      dts: 'src/auto-import.d.ts'
    })
  ],
  resolve: {
    alias: {
      '@': pathResolve('src')
    }
  }
});
