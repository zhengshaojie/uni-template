module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-essential',
    'plugin:@typescript-eslint/recommended',
    './.eslintrc-auto-import.json'
  ],
  overrides: [],
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    parser: '@typescript-eslint/parser',
  },
  plugins: ['vue', '@typescript-eslint'],
  rules: {
    'no-var': 'error', // 禁止使用 var
    'no-mixed-spaces-and-tabs': 'error', // 不能空格与tab混用
    quotes: [2, 'single'], // 使用单引号
    'vue/multi-word-component-names': 'off', // 驼峰警告
    '@typescript-eslint/no-explicit-any': 'off', // 使用any类型警告
    '@typescript-eslint/no-unused-vars': 'off', // 关闭检测未使用的变量,交由tsconfig.json中noUnusedLocals决定
    'no-undef': 0, // 不能有未定义的变量
  },
};
